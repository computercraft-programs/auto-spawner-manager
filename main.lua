if fs.exists("json") == false then
    shell.run("pastebin get 4nRg9CHU /lib/json")
end
os.loadAPI("/lib/json")

--[[
Check if a table contains the wanted value
Parameters:
    tab (table): the table to sort
    element (string/int): table key used to sort the table
Return:
    bool
]]
local function table_contains(tab, element)
    for _, value in pairs(tab) do
        if value == element then
            return true
        end
    end
    return false
end

--[[
Sort a table based on numerical value
Parameters:
    tab (table): the table to sort
    key (string/int): table key used to sort the table
    asc (bool): if the sorting must be ascending or not

Return:
    value: the value wanted
]]
local function sort_table(tab, sort_key, asc)
    tab = tab or error("find_item: missing [tab] parameter")
    sort_key = sort_key or error("find_item: missing [sort_key] parameter")
    if asc == nil then
        error("find_item: missing [asc] parameter")
    end

    table.sort(
        tab,
        function(a, b)
            if asc then
                return a[sort_key] < b[sort_key]
            else
                return a[sort_key] > b[sort_key]
            end
        end
    )
    return tab
end

local function save_config(tab)
    local path = fs.getDir(shell.getRunningProgram()) .. "config.json"
    local fh = fs.open(path, "w")
    fh.write(json.encodePretty(tab))
    fh.close()
end

local function load_config()
    local path = fs.getDir(shell.getRunningProgram()) .. "config.json"
    return json.decodeFromFile(path)
end

local function create_config()
    local path = fs.getDir(shell.getRunningProgram()) .. "config.json"
    local config = {}

    -- peripherals
    config.peripherals = {
        enderchest = "ender_chest",
        spawner = "auto_spawner",
        interface = "tileinterface",
        drawer = "storagedrawers_container_drawers",
        monitor = "monitor"
    }

    -- items
    config.items = {}

    term.clear()
    term.setCursorPos(1, 1)

    -- input redstone frequencies
    config.redstone = {}

    print("Entrez le cote de sortie redstone")
    config.redstone.side = read()

    print("Entrez la couleur du cable du spawner")
    config.redstone.spawner = read()

    print("Entrez la couleur du cable de la porte")
    config.redstone.door = read()

    -- input online player detector
    print("Entrez votre pseudo: ")
    config.player = read()

    print("Entrez l'adresse du serveur: ")
    config.server = read()

    save_config(config)
end

--[[
    Find the item with key value
    Parameters:
        p (peripheral): The inventory peripheral
        key (string): the key of the value (example: "display_name")
        value (?): the value wanted for the key
     
    Return:
        slot: the slot where the item is
]]
local function find_item(p, key, value)
    p = p or error("find_item: missing [p] parameter")
    key = key or error("find_item: missing [key] parameter")
    value = value or error("find_item: missing [value] parameter")

    local slots_list = {}
    local size = p.getInventorySize()

    for i = 1, size do
        local item = p.getStackInSlot(i)
        if item then
            if item[key] == value then
                slots_list[#slots_list + 1] = i
            end
        end
    end

    if #slots_list == 1 then
        return slots_list[1]
    elseif #slots_list == 0 then
        return false
    else
        return slots_list
    end
end

local function is_player_online()
    -- load the config
    local config = load_config()

    -- send the request to the api to get players list
    local str = http.get("http://api.mineaurion.com/v1/serveurs/" .. config.server).readAll()
    local obj = json.decode(str)

    if obj then
        if obj.joueurs then
            return table_contains(obj.joueurs, config.player)
        else
            return false
        end
    else
        return false
    end
end

local function get_door_state()
    local config = load_config()
    local side = config.redstone.side

    local rs_state = rs.getBundledInput(side)
    -- 0: spawner eteint, porte fermé
    -- 1: spawner eteint, porte ouverte
    -- 2: spawner allumé, porte fermé
    -- 3: spawner allumé, porte ouverte

    -- si la porte est ouverte
    if rs_state == 1 or rs_state == 3 then
        return true
    else
        return false
    end
end

local function get_spawner_state()
    local config = load_config()
    local side = config.redstone.side

    local rs_state = rs.getBundledInput(side)
    -- 0: spawner eteint, porte fermé
    -- 1 / white: spawner eteint, porte ouverte
    -- 2 / orange: spawner allumé, porte fermé
    -- 3 / orange + white: spawner allumé, porte ouverte

    -- si le spawner est allumé
    if rs_state > 1 then
        return true
    else
        return false
    end
end

local function open_door()
    local config = load_config()
    local side = config.redstone.side

    -- 0: spawner eteint, porte fermé
    -- 1 / white: spawner eteint, porte ouverte
    -- 2 / orange: spawner allumé, porte fermé
    -- 3 / orange + white: spawner allumé, porte ouverte

    -- si le spawner est allumé
    local freq
    if get_spawner_state() then
        freq = colors[config.redstone.spawner] + colors[config.redstone.door]
    else
        freq = colors[config.redstone.door]
    end

    rs.setBundledOutput(side, freq)
end

local function close_door()
    local config = load_config()
    local side = config.redstone.side

    -- 0: spawner eteint, porte fermé
    -- 1 / white: spawner eteint, porte ouverte
    -- 2 / orange: spawner allumé, porte fermé
    -- 3 / orange + white: spawner allumé, porte ouverte

    -- si le spawner est allumé
    local freq
    if get_spawner_state() then
        freq = colors[config.redstone.spawner]
    else
        freq = 0
    end
    rs.setBundledOutput(side, freq)
end

local function stop_spawner()
    local config = load_config()
    local side = config.redstone.side

    -- 0: spawner eteint, porte fermé
    -- 1 / white: spawner eteint, porte ouverte
    -- 2 / orange: spawner allumé, porte fermé
    -- 3 / orange + white: spawner allumé, porte ouverte

    -- si la porte est ouverte
    local freq
    if get_door_state() then
        freq = colors[config.redstone.door]
    else
        freq = 0
    end
    rs.setBundledOutput(side, freq)
end

local function start_spawner()
    local config = load_config()
    local side = config.redstone.side

    -- 0: spawner eteint, porte fermé
    -- 1 / white: spawner eteint, porte ouverte
    -- 2 / orange: spawner allumé, porte fermé
    -- 3 / orange + white: spawner allumé, porte ouverte

    -- si la porte est ouverte
    local freq
    if get_door_state() then
        freq = colors[config.redstone.spawner] + colors[config.redstone.door]
    else
        freq = colors[config.redstone.spawner]
    end

    if is_player_online() or global_master then
        rs.setBundledOutput(side, freq)
    end
end

local function add_item()
    local config = load_config()

    local p_drawer = peripheral.find(config.peripherals.drawer)
    p_drawer = p_drawer or error("add_items - No storage drawer found")

    term.clear()
    term.setCursorPos(1, 1)

    print("Mettez l'item' dans le premier slot")
    print("et le safari net dans le deuxieme slot")
    print("Appuyez sur Entree...")
    read()

    -- get items in drawers slot
    local item = p_drawer.getStackInSlot(3)
    local safari = p_drawer.getStackInSlot(4)

    -- check if items are here
    if (not safari) or (not item) then
        error("add_items - Missing item in storage drawer")
    end

    -- check if item is already in the config, delete it
    local index = 0
    for i = 1, #config.items do
        if config.items[i].item.display_name == item.display_name then
            index = i
        end
    end
    if index > 0 then
        config.items[index] = nil
    end

    -- create the table for the item and safari
    config.items[#config.items + 1] = {}

    --add item to keep in the config
    config.items[#config.items].item = item

    -- add safari net mob to the config
    local mob = safari.safari_net.captured
    if mob then
        config.items[#config.items].mob = mob
    else
        error("add_items - No safari net or empty safari net")
    end

    -- add the number of items to keep in the config
    print("Combien de " .. item.display_name .. " voulez-vous stocker: ")
    local n = tonumber(read()) or error("add_items - Quantity to keep is not a number")
    config.items[#config.items].keep = n

    --display priority of all the item in the config

    print("item : priorite")
    for i = 1, #config.items do
        if config.items[i].priority then
            print(config.items[i].item.display_name .. " : " .. config.items[i].priority)
        end
    end

    -- ask the priority for the item
    print("Priorite pour cet item: ")
    n = tonumber(read()) or error("add_items - priority is not a number")
    config.items[#config.items].priority = n

    -- sort items by descending priority (highest priority first)
    config.items = sort_table(config.items, "priority", false)

    -- save the config
    save_config(config)
    print("Item ajoute !")
end

local function manager()
    -- Load the config
    local config = load_config()

    -- Get the needed peripherals
    -- me interface
    local p_interface = peripheral.find(config.peripherals.interface)

    -- Ender chest
    local p_enderchest = peripheral.find(config.peripherals.enderchest)

    -- auto_spawner
    local p_spawner = peripheral.find(config.peripherals.spawner)

    -- sort items by priority (highest first)
    config.items = sort_table(config.items, "priority", false)

    -- check for each item if there is enough in ae system
    -- and get the index of the item with the highest priority and missing
    local ae_item
    local item_index = 0
    for i = 1, #config.items do
        -- Get the quantity of the item in the ae_system
        ae_item = p_interface.getItemDetail(config.items[i].item)

        -- If the item is present in ae system
        if ae_item then
            -- Check if there isn't enought of it
            if config.items[i].keep > ae_item.all().qty then
                item_index = i
                break
            end
        else
            -- if it's not in the ae set the index
            item_index = i
            break
        end
    end

    -- If there is an item to produce
    if item_index > 0 then
        -- Check if the safari net for the item is
        -- already in the spawner
        -- get the item in the spawner slot
        local safari = p_spawner.getStackInSlot(1)

        -- if there is a safari net, check if this is the right one
        if safari then
            -- if this is not the right safari net
            if safari.safari_net.captured ~= config.items[item_index].mob then
                -- remove the safari net from the spawner
                local amount = p_spawner.pushItem("up", 1, 1)

                -- check if the safari net has been removed
                if amount == 0 then
                    error("manager - can't remove safari net from spawner")
                end

                -- find the right safari net in enderchest
                for i = 1, p_enderchest.getInventorySize() do
                    safari = p_enderchest.getStackInSlot(i)

                    -- if the slot contain a safari net
                    if safari then
                        -- if this is the right safari net
                        if safari.safari_net.captured == config.items[item_index].mob then
                            -- push it into the spawner
                            amount = p_enderchest.pushItem("down", i, 1)

                            -- check if the safari net has been added
                            if amount == 0 then
                                error("manager - can't push safari net from the spawner")
                            end
                        end
                    end
                end
            end
        else
            -- if there is no safari net
            -- find the right safari net in enderchest
            for i = 1, p_enderchest.getInventorySize() do
                safari = p_enderchest.getStackInSlot(i)

                -- if the slot contain a safari net
                if safari then
                    -- if this is the right safari net
                    if safari.safari_net.captured == config.items[item_index].mob then
                        -- push it into the spawner
                        amount = p_enderchest.pushItem("down", i, 1)

                        -- check if the safari net has been added
                        if amount == 0 then
                            error("manager - can't push safari net from the spawner")
                        end
                    end
                end
            end
        end
    else
        -- if there is no item to produce
        -- if there is a safari net inside the spawner
        if p_spawner.getStackInSlot(1) then
            -- remove the safari net from the spawner
            local amount = p_spawner.pushItem("up", 1, 1)

            -- check if the safari net has been removed
            if amount == 0 then
                error("manager - can't remove safari net from spawner")
            end
        end
    end

    -- Global variables set
    if item_index > 0 then
        global_item = config.items[item_index].item.display_name
        global_keep = config.items[item_index].keep

        if ae_item then
            global_amount = ae_item.all().qty
        else
            global_amount = 0
        end
    else
        global_item = false
        global_keep = false
        global_amount = false
    end
end

local function menu()
    local config = load_config()
    local p_spawner = peripheral.find(config.peripherals.spawner)
    local p_monitor = peripheral.find(config.peripherals.monitor)

    local computer_size = {term.getSize()}
    local monitor_size = {p_monitor.getSize()}

    term.clear()
    p_monitor.clear()

    while true do
        local y = 0
        local rs_state = rs.getBundledInput("bottom")

        -- player state:
        y = y + 1
        if is_player_online() then
            term.setCursorPos(1, y)
            term.write("Joueur: connecte    ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Joueur: connecte    ")
        elseif global_master then
            term.setCursorPos(1, y)
            term.write("MASTER MODE          ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("MASTER MODE          ")
        else
            term.setCursorPos(1, y)
            term.write("Joueur: deconnecte    ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Joueur: deconnecte    ")
        end

        -- spawner state: on or off
        y = y + 1
        if get_spawner_state() then
            term.setCursorPos(1, y)
            term.write("Etat spawner: actif  ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Etat spawner: actif  ")
        else
            term.setCursorPos(1, y)
            term.write("Etat spawner: inactif")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Etat spawner: inactif")
        end

        -- door state: open or closed
        y = y + 1
        if get_door_state() then
            term.setCursorPos(1, y)
            term.write("Etat porte: ouverte")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Etat porte: ouverte")
        else
            term.setCursorPos(1, y)
            term.write("Etat porte: fermee ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("Etat porte: fermee ")
        end

        -- mob: name of the mob in safari net
        -- get the item in spawner
        local safari = p_spawner.getStackInSlot(1)

        -- if there is a safari net, get the mob's name
        y = y + 1
        if safari then
            term.setCursorPos(1, y)
            term.write("mob: " .. safari.safari_net.captured .. "         ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("mob: " .. safari.safari_net.captured .. "         ")
        else
            term.setCursorPos(1, y)
            term.write("mob: aucun")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("mob: aucun")
        end

        -- mode : automatic or manual
        y = y + 1
        if global_automatic then
            term.setCursorPos(1, y)
            term.write("mode: automatique")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("mode: automatique")

            y = y + 1
            term.setCursorPos(1, y)
            p_monitor.setCursorPos(1, y)
            if global_item then
                term.write("item: " .. global_item)
                p_monitor.write("item: " .. global_item)
            else
                term.write("item: aucun")
                p_monitor.write("item: aucun")
            end

            y = y + 1
            term.setCursorPos(1, y)
            p_monitor.setCursorPos(1, y)
            if global_amount and global_keep then
                term.write(global_amount .. " / " .. global_keep .. "         ")
                p_monitor.write(global_amount .. " / " .. global_keep .. "         ")
            else
                term.write("                              ")
                p_monitor.write("                              ")
            end
        else
            term.setCursorPos(1, y)
            term.write("mode: manuel     ")

            p_monitor.setCursorPos(1, y)
            p_monitor.write("mode: manuel     ")
        end

        -- CONTROLS
        term.setCursorPos(1, computer_size[2] - 2)
        term.write("Controles:")

        --spawner
        term.setCursorPos(1, computer_size[2] - 1)
        term.write("activer/desactiver le spawner: [S]")

        -- porte
        term.setCursorPos(1, computer_size[2])
        term.write("ouvrir/fermer la porte: [O]   Quitter: [Q]")

        sleep(2)
    end
end

local function controls()
    local config = load_config()
    local p_monitor = peripheral.find(config.peripherals.monitor)

    while true do
        local event = {os.pullEvent("key")}

        if keys.getName(event[2]) == "o" then
            if get_door_state() then
                close_door()
            else
                open_door()
            end
        elseif keys.getName(event[2]) == "s" then
            if get_spawner_state() then
                stop_spawner()
            else
                start_spawner()
            end
        elseif keys.getName(event[2]) == "q" then
            term.clear()
            p_monitor.clear()

            term.setCursorPos(1, 1)
            p_monitor.setCursorPos(1, 1)

            stop_spawner()
            break
        end
    end
end

-- GLOBAL VARIABLES
-- global_mob : mob being produced - name or false
global_item = ""
global_amount = 0
global_keep = 0

-- global_master: bypass the online detector
global_master = false
global_automatic = false

-- we create config if it's not there
local path = fs.getDir(shell.getRunningProgram())
if not fs.exists(path .. "config.json") then
    create_config()
end

local args = {...}

if args[2] == "master" then
    global_master = true
end

if args[1] == "auto" then
    global_automatic = true

    local config = load_config()
    if #config.items == 0 then
        add_item()
    end

    start_spawner()

    parallel.waitForAny(
        function()
            while true do
                manager()
                sleep(2)
            end
        end,
        menu,
        controls
    )
elseif args[1] == "manual" then
    global_automatic = false
    parallel.waitForAny(menu, controls)
elseif args[1] == "add" then
    add_item()
elseif args[1] == "config" then
    create_config()
else
    error(
        [[
    
Missing argument.
Usage:
- auto : spawner manager
- manual : manual spawner
- add: add an item and safari net to the spawner manager
- config: add or edit the config
    ]]
    )
end

term.clear()
term.setCursorPos(1, 1)
